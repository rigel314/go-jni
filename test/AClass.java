package test;

import java.lang.Class;
import java.lang.ClassLoader;
import java.lang.Exception;
import java.lang.Object;
import java.lang.String;

public class AClass {
	public static Object staticobj = new Object();
	public static boolean staticbl = true;
	public static byte staticbt = 'a';
	public static char staticch = 'b';
	public static short staticsh = 10;
	public static int staticint = 100;
	public static long staticlo = 1000;
	public static float staticfl = (float)1.1;
	public static double staticdb = 1000.0001;

	private static Object pstaticobj = new Object();
	private static boolean pstaticbl = true;
	private static byte pstaticbt = 'a';
	private static char pstaticch = 'b';
	private static short pstaticsh = 10;
	private static int pstaticint = 100;
	private static long pstaticlo = 1000;
	private static float pstaticfl = (float)1.1;
	private static double pstaticdb = 1000.0001;

	public Object obj = new Object();
	public boolean bl = false;
	public byte bt = 'c';
	public char ch = 'd';
	public short sh = 11;
	public int in = 101;
	public long lo = 1001;
	public float fl = (float)2.2;
	public double db = 1001.1001;

	private Object pobj = new Object();
	private boolean pbl = false;
	private byte pbt = 'c';
	private char pch = 'd';
	private short psh = 11;
	private int pin = 101;
	private long plo = 1001;
	private float pfl = (float)2.2;
	private double pdb = 1001.1001;

	public static boolean GetStaticBoolean() {
		return staticbl;
	}

	public static byte GetStaticByte() {
		return staticbt;
	}

	public static char GetStaticChar() {
		return staticch;
	}

	public static short GetStaticShort() {
		return staticsh;
	}

	public static int GetStaticInt() {
		return staticint;
	}

	public static void SetStaticInt(int val) {
		staticint = val;
	}

	public static Object GetStaticObject() {
		return staticobj;
	}

	public static void SetStaticBoolean(boolean val) {
		staticbl = val;
	}

	public void IncInt() {
		in = in + 1;
	}

	public int GetInt() {
		return in;
	}

	public boolean GetBoolean() {
    	return true;
	}

	public byte GetByte() {
    	return 127;
	}

	public char GetChar() {
    	return 65432;
	}

	public short GetShort() {
    	return 512;
	}

	public long GetLong() {
    	return 1L<<33;
	}

	public float GetFloat() {
    	return 4.321f;
	}

	public double GetDouble() {
    	return 5.4321;
	}

	public void SetInt(int val) {
		in = val;
	}

	public void AddInts(int val1, int val2) {
		in = val1 + val2;
	}

	public Class loadClass(String s) throws Exception {
		throw new Exception("AClass: loadClass exception");
	}

	public ClassLoader getClassLoader() {
		return this.getClass().getClassLoader();
	}
}
